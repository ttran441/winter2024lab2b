import java.util.Random;
import java.util.Scanner;

public class Wordle{
	
	//This method will take a random word out of an array
	public static String generateWord(){
		String[] words = {"PETAL", "CREAM", "HOUSE", "PASTY", "POUND", "PITCH", "SINCH", "FABLE", "SNAKE", "BEFIT", "CLOTH", 
		"RATED", "CREST", "CAKED", "WORST", "YEARS", "SCOUT", "LIGHT", "SADLY", "LOVED", "ADORE", "SHAME", "BIRTH", "PLASM",
		"BANJO", "OTHER", "STAND", "GAMER", "MEDIA", "SLIME", "SMORE", "OILED", "LARGE", "BLEND", "TRADE", "TRAIN", "CREST"};
		Random ran = new Random();
		return words[ran.nextInt(words.length)];
	}
	//This method will check if the letter guessed letter is in the word
	public static boolean letterInWord(String word, char letter){
		for (int i = 0; i < word.length(); i++){
			if (word.charAt(i) == letter){
				return true;
			}
		}
		return false;
	}
	//This method will make sure that the letter we want to find is in the position it should appear at
	public static boolean letterInSlot(String word, char letter, int position){
		if (position > word.length()){
			return false;
		}
		else if (word.charAt(position) == letter){
			return true;
		}
		else {
			return false;
		}
	}
	//This method compares two strings and will return an array of colours based on if the letters guessed at each position are correct or not
	public static String[] guessWord(String answer, String guess){
		String[] colours = {"white", "white", "white", "white", "white"};
		for (int i = 0; i < 5; i++){
			if (letterInWord(answer, guess.charAt(i))){
				colours[i] = "yellow";
				if (letterInSlot(answer, guess.charAt(i), i)){
					colours[i] = "green";
				}
			}
		}
		return colours;
	}
	//This method prints the 5 letters in the correct colours if they are guessed correctly or not
	public static void presentResults(String word, String[] colours){
		final String GREEN = "\u001B[32m";
		final String YELLOW = "\u001B[33m";
		final String WHITE = "\u001B[0m";
		for (int i = 0; i < word.length(); i++){
			if (colours [i] == "green"){
				System.out.print(GREEN + word.charAt(i) + " ");
			}
			else if (colours[i] == "yellow"){
				System.out.print(YELLOW + word.charAt(i) + " ");
			}
			else {
				System.out.print(WHITE + word.charAt(i) + " ");
			}
		}
		System.out.println(WHITE);
	}
	//This method asks the user to input a guess word
	public static String readGuess(){
		Scanner reader = new Scanner(System.in);
		System.out.println("Input a 5-letter guess word");
		String guess = reader.next();
		while (guess.length() != 5){
			System.out.println("Please input a valid 5 letter guess");
			guess = reader.next();
		}
		return guess.toUpperCase();
	}
	//This method takes in the target word and then starts the game
	public static void runGame(String word){
		String guess = readGuess();
		String[] letterColours = guessWord(word, guess);
		int i = 0;
		while (!guess.equals(word) && i < 5){
			letterColours = guessWord(word, guess);
			presentResults(guess, letterColours);
			guess = readGuess();
			i++;
		}
		if (!guess.equals(word) && i == 5){
			presentResults(guess, letterColours);
			System.out.println("You lost, the word was " + word.toLowerCase() + ", try again!");
		}
		if (guess.equals(word)){
			letterColours = guessWord(word, guess);
			presentResults(guess, letterColours);
			System.out.println("You guessed the word correctly, you win!");
		}
	}
}