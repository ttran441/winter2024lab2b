import java.util.Scanner;

public class WordGuesser{
	
	// The method checks all the letters in the word to see if the char 'c'
	// which was inputted by the user is present in the word, it then returns the
	// index of the character or -1 if the letter is not found
	public static int isLetterInWord(String word, char c){
		// used a for loop here to iterate through the characters
		for (int i = 0; i < word.length(); i++){ 
			
			// boolean to check if the letter is in the word
			boolean letterFound = c == word.charAt(i);
		
			// returns the position of the letter within the word
			if (letterFound){
				return i;
			}
		}
		
		// returns - 1 if the letter is not found within the word
		return -1; 
	}
	
	// This method checks every single letter to see if it was guessed or not
	// if the letter is still not guessed, a '_' will be in its place
	// if the letter is guessed, the letter at that position will appear
	public static void printWord(String word, boolean[] lettersFound){
		
		// The letters start out unguessed
		char[] letters = {'_','_','_','_'};
		
		// Iterates through the array of booleans and checks which ones
		// are true, if true, it the method replaces the unguessed letter
		// with the real one
		for (int i = 0; i < lettersFound.length; i++) {
			
			// Checks if the letter is found in this position and changes
			// the unguessed letter to the actual letter
			if (lettersFound[i]) {
				letters[i] = word.charAt(i);
			}
			
		}
		
		// Prints out the result of the method
		System.out.println("Your result is " + letters[0] + letters[1] + letters[2] + letters[3]);
	}
	
	// This method will take letter inputs from the user, and then use the isLetterInWord and printWord methods to determine if the letters are in the word or not
	public static void runGame(String word){
		Scanner reader = new Scanner(System.in);
		
		// Initialized an array for each letter to see if they were guessed
		boolean[] lettersFound = {false, false, false, false};
		final int LETTER_NOT_FOUND = -1;
		int missCounter = 0;
		boolean win = false;
		boolean lost = false;
		while (!lost && !win) {
			System.out.println("");
			System.out.println("Guess a letter: ");
			char guess = reader.next().charAt(0);
			guess = Character.toUpperCase(guess);
			
			// Checks if the letter is in the word
			// if not missCounter is incremented
			// if yes then the position in the array is set to true
		    if (isLetterInWord(word, guess) == LETTER_NOT_FOUND) {
		        missCounter++;
		    }
			else {
				int letterPos = isLetterInWord(word, guess);
				lettersFound[letterPos] = true;
			}
			
			printWord(word, lettersFound);
			
			// Checks again to see if the user has won or lost
			win = lettersFound[0] && lettersFound[1] && lettersFound[2] && lettersFound[3];
			lost = missCounter == 6;
		}
		if (win) {
			System.out.println("");
			System.out.println("Congratulations, you win!");
			}
		else {
			System.out.println("");
			System.out.println("Too many wrong guesses, you lose! The word was " + word + "!");
		}
	}
}