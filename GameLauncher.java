import java.util.Scanner;

public class GameLauncher {
	
	// Asks the user for an input to run either the Hangman game or the Wordle game	
	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Welcome Player!");
		System.out.println("Input \'1\' to run the Hangman Game, Input \'2\' to run the Wordle game");
		
		int input = reader.nextInt();
		
		while (input > 2 || input < 1) { // Checks if the user input is valid or not
			
			System.out.println("Please input a valid answer (\'1\' for Hangman, \'2\' for Wordle)");
			
			input = reader.nextInt();
			
		}
		
		if (input == 1) {
			
			runHangman(reader);
			
		}
		
		else {
			
			runWordle();
			
		}
		
	}
	
	//This method takes in the word that will be guessed, then runs the Hangman game
	public static void runHangman(Scanner reader) {
		
		System.out.println("Input your 4-letter word here: ");
		String word = reader.next().toUpperCase();
		System.out.print("\033[H\033[2J");
		WordGuesser.runGame(word);
		
	}
	
	// This method generates a word then uses that word to run the Wordle game
	public static void runWordle() {
		
		String word = Wordle.generateWord();
		Wordle.runGame(word);
		
	}
	
}